//
//  ViewController.swift
//  Secretos
//
//  Created by formador on 8/2/17.
//  Copyright © 2017 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var secreto: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(ajustarTeclado), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(ajustarTeclado), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func ajustarTeclado(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == NSNotification.Name.UIKeyboardWillHide {
            secreto.contentInset = UIEdgeInsets.zero
        } else {
            secreto.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        secreto.scrollIndicatorInsets = secreto.contentInset
        
        let selectedRange = secreto.selectedRange
        secreto.scrollRangeToVisible(selectedRange)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

